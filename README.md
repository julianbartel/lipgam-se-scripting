# LipGam's Space Engineers Ingame Scripts
Some scripts for automating our Space Engineers world.

## Getting started
- Download the latest released scripts from [Releases](https://github.com/julianbartel/lipgam-se-scripting/releases)
- Copy the content of the package to your Space Engineers ingame scripts folder (eg. C:\Users\YourWindowsUserName\AppData\Roaming\SpaceEngineers\IngameScripts\local).
- Select the scripts from the ingame workshop.
- Configure your blocks in the terminal according to the scripts needs. See the Wiki for descriptions of each script.

Take a look at our  [Wiki](https://github.com/julianbartel/lipgam-se-scripting/wiki) to read how to use the scripts.

## Getting started with development
What you need:
- Visual Studio 2017
- Knowledge about software development with C#
